const MongoClient = require('../libs/mongo-client');
const CustomError = require('../helpers/custom-error');

class BusinessUnit {
  /**
   * Get business unit by arrondissement
   * @param {Object} params.arrondissement Arrondissement Object
   */
  static async getBusinessUnitByArrondissement({ arrondissement }) {
    if (!arrondissement || !arrondissement.fields || !arrondissement.fields.code_arrondissement) {
      throw new CustomError(400, 'No arrondissement code found.');
    }

    const {
      code_arrondissement: codeArrondissement
    } = arrondissement.fields;

    const businessUnit = await MongoClient.db.collection('businessUnits').findOne({
      arrondissements: Number(codeArrondissement)
    });

    // As arrondissement given as parameter is a verified french arrondissement
    // If no businessUnit found from businessUnit collection name it 'Autres'
    const result = {
      businessUnit: {
        name: (businessUnit && businessUnit.name) || 'Autres'
      },
      codeArrondissement
    };

    return result;
  }
}

module.exports = BusinessUnit;
