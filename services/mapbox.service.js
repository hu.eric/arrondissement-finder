const CustomError = require('../helpers/custom-error');

class MapBoxService {
  /**
   * Get coordinates from MapBox response object
   * @param {Object} mapboxResponse MapBox response object
   */
  static getCoordinates(mapboxResponse) {
    let coordinates = null;

    const { features } = mapboxResponse;

    if (!features || !features.length) {
      throw new CustomError(400, 'No address found.');
    }

    // Get the most relevant data
    const address = features[0];

    coordinates = address && address.geometry && address.geometry.coordinates;

    return coordinates;
  }
}

module.exports = MapBoxService;
