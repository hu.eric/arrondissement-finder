const MongoClient = require('../libs/mongo-client');
const CustomError = require('../helpers/custom-error');

class ArrondissementService {
  /**
   * Get arrondissement with coordinates
   * @param {Array} params.coordinates Array of coordinates
   */
  static getArrondissement({ coordinates }) {
    if (!coordinates || !(Array.isArray(coordinates)) || coordinates.length !== 2) {
      throw new CustomError(400, 'Malformed address coordinates.');
    }

    return MongoClient.db.collection('arrondissements').findOne({
      'fields.geo_shape': {
        $geoIntersects: {
          $geometry: {
            type: 'Point',
            coordinates
          }
        }
      }
    });
  }
}

module.exports = ArrondissementService;
