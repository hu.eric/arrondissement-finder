const express = require('express');
const log4js = require('log4js');
const mongoClient = require('./libs/mongo-client');
const router = require('./routes');
const errorHandler = require('./middlewares/error-handler');

const app = express();
const port = process.env.PORT || 8080;
const logger = log4js.getLogger('app');

(async () => {
  logger.level = 'info';

  // Catch uncaughtExceptions and unhandledRejection
  process
    .on('unhandledRejection', (reason) => {
      logger.fatal('Unhandled Rejection at', reason);
    })
    .on('uncaughtException', (err) => {
      logger.fatal('Uncaught Exception thrown', err);
      process.exit(1);
    });

  await mongoClient.connect();

  // Initiate routes
  router(app);

  // Initialize middleware
  app.use(errorHandler);

  app.listen(port, () => logger.info(`Arrondissement-finder app listening on port ${port}!`));
})();
