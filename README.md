# arrondissement-finder

Retrive associated "arrondissement" and business unit for a given address.


## Information
- Node 10.x

## Commands

### Install packages

```
npm install
```

### Run Node app

```
npm start
```

## Routes
### Worker
---
#### Summary

| HTTP METHOD | URL                       | Description       |
| ----------- | ------------------------- | ----------------- |
| GET         | /api/worker/business-unit | Get business unit |

#### [GET] /api/worker/business-unit

*  **URL Params**

   **Required:**

   `address=[string]`<br />

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "success": true,
        "message": "Business unit successfully retrieved",
        "data": {
            "businessUnit": {
                "name": "Ile de France"
            },
            "codeArrondissement": "751"
        }
    }
    ```

* **Failure Response:**

  * **Code:** 400 <br />
    **Content:**
    ```
    {
        "success": false,
        "message": "Failed to retrieve business unit.",
        "errors": [
            {
                "msg": "No business unit found."
            }
        ]
    }
    ```