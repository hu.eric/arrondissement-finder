class CustomError extends Error {
  constructor(statusCode, ...args) {
    super(...args);

    this.statusCode = statusCode;
  }
}

module.exports = CustomError;
