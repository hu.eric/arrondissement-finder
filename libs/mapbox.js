const request = require('request-promise-native');

const PUBLIC_TOKEN = 'pk.eyJ1IjoiZXJpY2h1OCIsImEiOiJjanlvcDZnM2owMmI5M251ZjVibHRqNTEzIn0.rv61HBrGO-37V_fzxpSmkw';
const MAPBOX_URL = 'https://api.mapbox.com';

class MapBox {
  constructor() {
    const config = {
      accessToken: PUBLIC_TOKEN
    };

    this.init(config);
  }

  init({ accessToken }) {
    this.url = MAPBOX_URL;
    this.accessToken = accessToken;
  }

  getGeocodeFromAddress({ accessToken = null, address }) {
    const qs = {
      access_token: accessToken || this.accessToken
    };
    const formatedAddress = encodeURIComponent(address);

    return request.get({
      url: `${this.url}/geocoding/v5/mapbox.places/${formatedAddress}.json`,
      qs,
      json: true
    });
  }
}

module.exports = new MapBox();
