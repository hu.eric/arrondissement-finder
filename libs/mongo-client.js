const MongoC = require('mongodb').MongoClient;

const uri = 'mongodb://finder:bX3hh3aWEYv7wrN@ds131721.mlab.com:31721/heroku_jbh7q65r';
const dbName = 'heroku_jbh7q65r';


class MongoClient {
  constructor() {
    this.client = new MongoC(uri, { useNewUrlParser: true });
  }

  async connect() {
    await this.client.connect();

    this._db = this.client.db(dbName);
  }

  get db() {
    if (!this._db) {
      throw new Error('Mongo client not connected to a database !');
    }

    return this._db;
  }

  close() {
    if (this.client) {
      this.client.close();
    }
  }
}

module.exports = new MongoClient();
