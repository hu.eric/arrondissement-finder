const { query } = require('express-validator');

const getBusinessUnit = [
  query('address')
    .not().isEmpty()
    .trim()
];

module.exports.getBusinessUnit = getBusinessUnit;
