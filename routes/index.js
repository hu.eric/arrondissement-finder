const validate = require('../middlewares/validation');

const WorkerSchema = require('../schemas/worker');
const Worker = require('../controllers/worker');

module.exports = (app) => {
  app.get('/api/worker/business-unit', validate(WorkerSchema.getBusinessUnit), Worker.getBusinessUnit);
};
