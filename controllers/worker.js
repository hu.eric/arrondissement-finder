const MapBox = require('../libs/mapbox');
const CustomError = require('../helpers/custom-error');

const MapBoxService = require('../services/mapbox.service');
const ArrondissementService = require('../services/arrondissement.service');
const BusinessUnitService = require('../services/business-unit.service');

class Worker {
  /**
   * Get business unit for a worker address
   */
  static async getBusinessUnit(req, res, next) {
    const params = {
      address: req.query.address
    };
    let result;

    try {
      // Call to MapBox API for address objects
      const mapboxResults = await MapBox.getGeocodeFromAddress(params);

      // Parse and retrive coordinates
      const coordinates = MapBoxService.getCoordinates(mapboxResults);

      // Find associated arrondissement from our database
      const arrondissement = await ArrondissementService.getArrondissement({ coordinates });

      // Find associated business unit from arrondissement code
      const data = await BusinessUnitService.getBusinessUnitByArrondissement({ arrondissement });

      // Checking data
      if (!data || !data.businessUnit || !data.businessUnit.name) {
        throw new CustomError(400, 'No business unit found.');
      }

      result = {
        success: true,
        message: 'Business unit successfully retrieved',
        data
      };
      res.status(200).json(result);
    } catch (error) {
      if (!(error instanceof CustomError)) {
        return next(error);
      }

      result = {
        success: false,
        message: 'Failed to retrieve business unit.',
        errors: [{
          msg: error.message
        }]
      };
      res.status(error.statusCode || 500).json(result);
    }
  }
}

module.exports = Worker;
