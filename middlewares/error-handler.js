function errorHandler(err, _, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  return res.status(500).json({
    error: err
  });
}

module.exports = errorHandler;
